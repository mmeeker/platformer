﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraMovement : MonoBehaviour
{
    public GameObject player;
    public float smoothSpeed = 0.125f;
    public Vector3 offset;

    void FixedUpdate()
    {
        if (player != null)
        {
            Vector3 targetPos = player.transform.position + offset;
            Vector3 smoothedPos = Vector3.Lerp(transform.position, targetPos, smoothSpeed);

            transform.position = smoothedPos;
        }
    }
}
