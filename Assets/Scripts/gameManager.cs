﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class gameManager : MonoBehaviour
{

    public int maxHealth, health;
    string Health, MaxHealth;
    public int recruitValue = 100;
    public int power = 1;
    public bool flipped = false;
    public bool attackMode = true;

    public bool pauseState = false;

    public GameObject background;
    public GameObject player;
    public Text infoText;
    public Text endText;
    public Text pauseText;

    public List<string> allies = new List<string>();

    public GameObject[] panels = new GameObject[5];
    

    // Start is called before the first frame update
    void Start()
    {   
        this.Health = health.ToString();
        this.MaxHealth = maxHealth.ToString(); ;

        int rx, ry, rz;
        Vector3 newPos;
        Quaternion newRot;
        infoText.text = "max health: " + MaxHealth + "\ncurrent health: " + Health + "\npower: " +power;
        for(int i = 0; i < 140; i++)
        {
            rx = Random.Range(-13, 440);
            ry = Random.Range(-5, 50);
            rz = Random.Range(0, 90);
            newPos = new Vector3(rx, ry, 100);
            newRot = Quaternion.Euler(new Vector3(0, 0, rz)); 
            Instantiate(background, newPos, newRot);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (health > maxHealth)
            health = maxHealth;

        if (Input.GetKeyDown(KeyCode.P)){            
            Pause();
        }
        // if (Input.GetButtonDown("Flip Ally"))
        // {
        //     flipAllies = !flipAllies;
        //     float newX, diff;
        //     foreach (GameObject n in allies)
        //     {
        //         diff = Mathf.Abs(player.transform.position.x - n.transform.position.x);
        //         if (flipAllies)
        //             newX = player.transform.position.x - diff;
        //         else
        //             newX = player.transform.position.x + diff;

        //         n.transform.position = new Vector3(newX, n.transform.position.y, n.transform.position.z);
        //     }
        // }
        if(health <= 0 && player != null)
        {
            Destroy(player, 0.3f);
            player.transform.DOScale(new Vector3(1f, -1f, 1f), .3f * 3.5f);
            // foreach(GameObject n in allies)
            // {
            //     Destroy(n, 0.3f);
            //     n.transform.DOScale(new Vector3(1f, -1f, 1f), .3f * 3.5f);
            // }
            endText.enabled = true;
        }

        Health = health.ToString();
        MaxHealth = maxHealth.ToString(); ;
        infoText.text = "health: " + Health + "/" + MaxHealth + "\npower: " + power;
    }

    void Pause()
    {
        if(pauseState)
            Time.timeScale = 1;
        else
            Time.timeScale = 0;
        pauseText.enabled = !pauseText.enabled;
        pauseState = !pauseState;
    }
}