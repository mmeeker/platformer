﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player : MonoBehaviour
{
    float buttonTime = 0.5f;
    public float jumpVelocity;
    float jumpTime;
    bool jumping;
    float jumpHeight = 8;
    float cancelRate = 100;
    bool jumpCancelled;
    int maxJumpCount = 1;
    int jumpCount = 1;

    private Animator anim;

    public float speed = 10f;
    public gameManager gm;
    public spikeCollision spikeScript;
    public Rigidbody rb;
    
    [SerializeField]
    GameObject body;
    [SerializeField]
    GameObject Spikes;
    [SerializeField]
    GameObject Bumps;

    float gravityScale = 4;

void Start()
    {
        rb = GetComponent<Rigidbody>();
        spikeScript = Spikes.GetComponent<spikeCollision>();
        anim = GetComponent<Animator>();
    }

void Update()
    {
      //Secondary player controlled respawner. This is just incase the player jumps off and misses the respawn collider, somehow.
      if(transform.position.y < -100)
        {
            gm.health--;
            transform.position = new Vector3(0, 1.5f);
            rb.velocity = new Vector3(0f,0f,0f);
        }  

        //checks if game is paused
        if(!gm.pauseState){

            //code to allow for variable height jumping
            if (Input.GetKeyDown(KeyCode.UpArrow) && jumpCount > 0)
            {   
                jumpCount--;
                jumpVelocity = Mathf.Sqrt(jumpHeight * -2 * Physics.gravity.y * gravityScale);                        
                rb.AddForce(new Vector2(0, jumpVelocity), ForceMode.Impulse);
                body.transform.DOScale(new Vector3(0.5f, 1.75f, 1f), 0.25f).SetLoops(2, LoopType.Yoyo);
                jumping = true;
                jumpCancelled = false;
                jumpTime = 0;
                
            }
            //determines whether we are still jumping for not. i think
            if (jumping)
            {
                jumpTime += Time.deltaTime;
                if (Input.GetKeyUp(KeyCode.UpArrow))
                {
                    jumpCancelled = true;
                }
                if (jumpTime > buttonTime)
                {
                    jumping = false;
                }
            }
                        
            //this is for attacking/recruiting the enemies
            if(Input.GetKeyDown(KeyCode.Space))      
            {       
                if(gm.attackMode && !anim.GetBool("isAttacking")){
                    anim.SetTrigger("Attacking");
                    anim.SetBool("isAttacking", true);           
                    print("Attack!!");
                }
                else if(!gm.attackMode && !anim.GetBool("isRecruiting")){                
                    anim.SetTrigger("Recruiting");
                    anim.SetBool("isRecruiting", true);
                    print("join me! ... Please?");
                }
            }

            //switches bewteen attacking and recruiting 
            if(Input.GetButtonDown("Mode Change")){       
                if(!anim.GetBool("isRecruiting") && !anim.GetBool("isAttacking")) {   
                    if(gm.attackMode){
                        Bumps.SetActive(true);
                        Spikes.SetActive(false);
                    }
                    else{
                        Bumps.SetActive(false);
                        Spikes.SetActive(true);
                    }
                    gm.attackMode = !gm.attackMode;
                }
            }

            //flips the player around so that the 'weapon' is facing the other direction
            if (Input.GetButtonDown("Flip"))
            {
                gm.flipped = !gm.flipped;
                body.transform.Rotate(new Vector3(0, 1), 180);
            }
        }
    }

void FixedUpdate()
    {
        
        rb.AddForce(Physics.gravity * (gravityScale - 1) * rb.mass);

        //Store user input as a movement vector
        Vector3 m_Input = new Vector3(Input.GetAxis("Horizontal"), 0, 0);

        //Apply the movement vector to the current position, which is
        //multiplied by deltaTime and speed for a smooth MovePosition
        rb.MovePosition(transform.position + m_Input * Time.deltaTime * speed);

        //part that allows for smaller jumps. i think
        if(jumpCancelled && jumping && rb.velocity.y > 0)
        {
            rb.AddForce(Vector2.down * cancelRate);
        }           
    }

//lets us know that attacking/recruiting is done. used in spike(recruit)Collision for when attack connects.
void stopAnimation()
{
    if(anim.GetBool("isAttacking"))
        anim.SetBool("isAttacking", false);
    
    if(anim.GetBool("isRecruiting"))
        anim.SetBool("isRecruiting",false);
}

//ground detection for jumping
void OnTriggerEnter(Collider other)
    {
        if(other.tag == "ground" && !jumping)
            jumpCount = maxJumpCount;
    }

/*
TO DO:
rename stuff: befriend to recruit, flip to mode switch? (done i think)
rework recruit mechanics
clean up code
add comments to code
fix the collisions when attacking. if the collision boxes overlap before you attack then you try to attack it does nothing, 
    since the collision can't enter something it is already inside of.
(multi) jump + (multi) attack = jank (done i think)
*/

}