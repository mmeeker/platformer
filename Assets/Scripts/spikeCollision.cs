﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class spikeCollision : MonoBehaviour
{
    private Animator anim;
    public gameManager gm;
    public GameObject enemy;
    private enemy enemyScript;

    public bool isAttacking = false;

 void Start()
    {        
        anim = GetComponentInParent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        enemy = other.gameObject;
        enemyScript = other.gameObject.GetComponent<enemy>();

        if (other.gameObject.CompareTag("enemy") && !enemyScript.ally && anim.GetBool("isAttacking"))
        {
            float offset;
            if (gm.flipped)
            {
                enemyScript.goingLeft = true;
                offset = -1.75f;
            }
            else
            {
                enemyScript.goingLeft = false;
                offset = 1.75f;
            }
            enemyScript.health -= gm.power;
            enemy.transform.position = new Vector3(enemy.transform.position.x + offset, enemy.transform.position.y, enemy.transform.position.z);
            
            if (enemyScript.health <= 0)
            {
                Destroy(other.gameObject, 0.3f);
                other.gameObject.transform.DOScale(new Vector3(1f, -1f, 1f), .3f * 3.5f);
            }
        }
    }
}
