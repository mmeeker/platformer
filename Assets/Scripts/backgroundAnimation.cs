﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class backgroundAnimation : MonoBehaviour
{
    int num;
    float localXR, localYR, localZR;
    float localXP, localYP, localZP;
    float Speed, rx, ry;

    void Start()
    {
        num = Random.Range(1,4);
        Speed = Random.Range(2f,5f);

        localXR = transform.rotation.x;
        localYR = transform.rotation.y;
        localZR = transform.rotation.z;

        if(num == 1)
        {//moving and rotating     
            rx = Random.Range(-25f, 25f);
            ry = Random.Range(-15f, 15f);

            localXP = transform.position.x;
            localYP = transform.position.y;
            localZP = transform.position.z;
            
            transform.DORotate(new Vector3(localXR, localYR, localZR + 180), Speed).SetLoops(-1, LoopType.Yoyo);
            transform.DOMove(new Vector3(localXP + rx, localYP + ry, localZP), Speed, false).SetLoops(-1, LoopType.Yoyo);
        }         
        else if(num == 2)
        {//expanding 
            transform.localScale = new Vector3(0.3f, 0.3f, 0.3f); 
            transform.DOScale(1.5f, Speed).SetLoops(-1, LoopType.Yoyo);
        }
        else//rotating
            transform.DORotate(new Vector3(localXR, localYR, localZR + 90),Speed-1).SetLoops(-1,LoopType.Incremental);
    }
}