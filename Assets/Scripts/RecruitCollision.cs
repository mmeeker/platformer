using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class RecruitCollision : MonoBehaviour
{
    float recruitRate;
    public gameManager gm;
    public GameObject enemy;
    private enemy enemyScript;
    private Animator anim;

 void Start()
    {        
        anim = GetComponentInParent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        enemy = other.gameObject;
        enemyScript = other.gameObject.GetComponent<enemy>();

        int r = Random.Range(gm.recruitValue / 2, gm.recruitValue);

        if (other.gameObject.CompareTag("enemy"))
        {
            float offset;
            if (gm.attackMode)  //used to be gm.flipAllies
                offset = gm.allies.Count + 0.5f;
            else
                offset = -gm.allies.Count -0.5f;
            recruitRate = r * enemyScript.recruitChance;
            //you have about a 47% chance of success without any additional allies
            //with 1 ally you have about a 60% chance of success

            if (recruitRate > 43.5 && !enemyScript.ally && gm.allies.Count < 5 && anim.GetBool("isRecruiting"))
            {//need to add a way to remove from ally list in game

                print(recruitRate + " , " + r);
                print("success");
                gm.power++;
                gm.maxHealth--;
                activateInPanel(other.gameObject.name);
                gm.allies.Add(other.gameObject.name);
                
                gm.recruitValue += 10;

                Destroy(other.gameObject, 0.3f);
                other.gameObject.transform.DOScale(new Vector3(1f, -1f, 1f), .3f * 3.5f);
     
                //enemy.transform.position = new Vector3(transform.position.x - offset - offset, transform.position.y, transform.position.z);
                //enemyScript.ally = true;
            }
            else if (!enemyScript.ally)
            {
                print("failed");
               // gm.health--;
                enemy.transform.position = new Vector3(enemy.transform.position.x + 0.5f, enemy.transform.position.y, enemy.transform.position.z);
            }
        }
    }

void activateInPanel(string name){
    GameObject temp = gm.panels[gm.allies.Count];
    Image[] child = temp.GetComponentsInChildren<Image>(true);

    foreach (var item in child)
    {
        if (item.name == name)
            item.enabled = true;            
    }
}



}

