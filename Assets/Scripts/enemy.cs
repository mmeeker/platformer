﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class enemy : MonoBehaviour
{
    public int health = 3;
    float speed = 5.5f;
    float jumpVelocity = 21.5f;
    float jumpRatio = 2.5f / 4f;
    float jumpTime = 0;
    public bool goingLeft = false;
    public Vector2 startPosition, endPosition;

    public bool ally = false;
    public float recruitChance = 0.6f;

    Rigidbody rb;

    public GameObject player;
    private Player playerScript;
    public gameManager gm;

    float gravityScale = 3.5f;

    [SerializeField]
    GameObject body;

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        transform.position.Set(startPosition.x, startPosition.y, 0);
        playerScript = player.GetComponent<Player>();
    }

    void FixedUpdate()
    {
        Vector3 dir = new Vector3();
        rb.AddForce(Physics.gravity * (gravityScale - 1) * rb.mass);
        // if (gm.allies.Contains(this.gameObject))
        // {
        //     ally = true;
        // }

        if (ally && player != null)
        {
            float horizontl_move = Input.GetAxis("Horizontal");
            dir = new Vector3(horizontl_move, 0, 0);
            speed = playerScript.speed;
            jumpVelocity = playerScript.jumpVelocity;

            //playerScript.rb.velocity.y == 0 &&
            if ( Input.GetButtonDown("Jump"))
            {
                rb.velocity = Vector3.up * jumpVelocity;
                jumpTime = 0;

                body.transform.DOScale(new Vector3(1 / 1.5f, 1.5f, 1f), 0.5f * 0.75f / 2f).SetLoops(2, LoopType.Yoyo);
            }

            if (Input.GetButtonUp("Jump") && jumpTime < 0.2f)
                rb.velocity *= jumpRatio;

            if (rb.velocity.y != 0)
                jumpTime += Time.deltaTime;

            transform.Translate(dir * speed * Time.deltaTime);

            jumpVelocity = 15;
        }
        else
        {
            if (this.transform.position.x < endPosition.x && !goingLeft && startPosition != endPosition)
            {
                dir = new Vector3(1, 0, 0);
            }
            else
                goingLeft = true;
            if (this.transform.position.x > startPosition.x && goingLeft && startPosition != endPosition)
            {
                dir = new Vector3(-1, 0, 0);
            }
            else
                goingLeft = false;
            if (startPosition == endPosition)
                dir = new Vector3(0, 0, 0);

            transform.Translate(dir * speed * Time.deltaTime);
        }
        
        if (rb.velocity.y == 0 && name == "enemy")
        {
            rb.velocity = Vector3.up * jumpVelocity;

            body.transform.DOScale(new Vector3(1 / 1.5f, 1.5f, 1f), 0.5f * 0.75f / 2f).SetLoops(2, LoopType.Yoyo);
        }
    }
}