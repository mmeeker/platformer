﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class playerCollision : MonoBehaviour
{
    public gameManager gm;
    public GameObject enemy;
    private enemy enemyScript;
    public GameObject player;
   

    private void OnTriggerEnter(Collider other)
    {
        
        enemy = other.gameObject;
        enemyScript = other.gameObject.GetComponent<enemy>();
        if (other.gameObject.CompareTag("enemy") && !enemyScript.ally)
        {
            print("hurt from enemy");
            enemy.transform.position = new Vector3(enemy.transform.position.x + 0.5f, enemy.transform.position.y, enemy.transform.position.z);
            player.transform.position = new Vector3(player.transform.position.x - 1f, player.transform.position.y, player.transform.position.z);
           // gm.health--;
        }
    }
}
