﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class respawn : MonoBehaviour
{
    public gameManager gm;
    public GameObject player;
    GameObject enemy;
    private enemy enemyScript;


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player)
        {
            gm.health--;
            player.transform.position = new Vector3(0, 1.5f);
        }        
        else if(other.gameObject.name == "enemy")
        {
            enemy = other.gameObject;
            enemyScript = other.gameObject.GetComponent<enemy>();
            enemy.transform.position = enemyScript.startPosition;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
